using Microsoft.AspNetCore.Mvc;
using POC1.DAO;
using POC1.Data;
using POC1.Servicos;

namespace POC1.Controllers
{
    public abstract class BaseController : Controller
    {
        
        protected ProdutoRepository prodRepository;
        protected SetorRepository setorRepository; 
        protected Filtro filtro;

        public BaseController()
        {
            
        }
        public BaseController(LojaContext context)
        {
            filtro = new Filtro(context);
            prodRepository = new ProdutoRepository(context);
            setorRepository = new SetorRepository(context);
        }

    }
}