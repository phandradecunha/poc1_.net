using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using POC1.Models;

namespace POC1.Controllers
{
    public class ErrorController : BaseController
    {
        Dictionary<int, ErrorViewModel> dicionario = new Dictionary<int, ErrorViewModel>()
            {
                { 0, new ErrorViewModel {Message = " Erro indefinido."}}, 
                { 1, new ErrorViewModel {Message = " ID com valor nulo." } },
                { 2, new ErrorViewModel {Message = " Valor do produto nulo." } },
                { 3, new ErrorViewModel {Message = " O produto não foi encontrado." } },
                { 4, new ErrorViewModel {Message = " O setor não foi encontrado." } },
            };

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Erro(int erro = 0)
        {
            
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                                             Message = dicionario[erro].Message});
        }
    }
}