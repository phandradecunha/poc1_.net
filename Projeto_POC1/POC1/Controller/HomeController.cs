﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using POC1.Data;
using POC1.Servicos;

namespace POC1.Controllers
{
    public class HomeController : BaseController
    {

        public HomeController(LojaContext context):base(context)
        {

        }

        public  IActionResult Index(string setor = "Setor", string nome=null, int pag=1)
        {  
            
            try{
                ViewData["Setores"] = new SelectList(setorRepository.Consultar(), "Nome", "Nome");

                ViewData["Setor"] = setor;
                ViewData["Nome"] = nome; 
                
                var query = filtro.Filtrar(nome,setor);
                
                ViewBag.TotalPaginas = Paginacao.ContaLista(query);

                return View(Paginacao.NumeroItems(query,pag));
            }
            catch
            {
                return RedirectToAction("Erro", "Error", new {erro = ""});
            } 
        }
    }
}

