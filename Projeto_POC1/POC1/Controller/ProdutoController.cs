using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using POC1.Data;
using POC1.Models;
using POC1.Servicos;

namespace POC1.Controllers
{
    public class ProdutoController : BaseController
    {

        public ProdutoController(LojaContext context):base(context)
        {

        }

        public IActionResult Index(int pag=1, string nome=null, string setor="Setor")
        {
            ViewData["Setores"] = new SelectList(setorRepository.Consultar(), "Nome", "Nome");

            ViewData["Setor"] = setor;
            ViewData["Nome"] = nome;

            var query = filtro.Filtrar(nome,setor);

            ViewBag.TotalPaginas = Paginacao.ContaLista(query); 
            return View("Index",Paginacao.NumeroItems(query, pag));
        }
        
        public async Task<IActionResult> Details(int? id)
        {
           if (id == null)
                return RedirectToAction("Erro", "Error", new {erro = 1});

           var produto = await prodRepository.Detalhes(id);

           if (produto == null)
                return RedirectToAction("Erro", "Error", new {erro = 2});

           return View(produto);
        }

        public IActionResult Create()
        {
            ViewData["idSetor"] = new SelectList(setorRepository.Consultar(), "idSetor", "nome");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("idProd,nome,desc,preco,idSetor")] Produto produto)
        {   
            try
            {
                
                if (ModelState.IsValid && Formulario.ValidaPreco(produto))
                {
                    await prodRepository.Criar(produto);
                    return RedirectToAction(nameof(Index));
                }

                ViewData["idSetor"] = new SelectList(setorRepository.Consultar(), "idSetor", "nome", produto.SetorId);
                return View(produto);
            }
            catch
            {
                return RedirectToAction("Erro","Error", new{ erro = ""});
            } 

        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("Erro", "Error", new {erro = 1});

            var produto = await prodRepository.Detalhes(id);

            if (produto == null)
                return RedirectToAction("Erro", "Error", new {erro = 2});

            ViewData["idSetor"] = new SelectList(setorRepository.Consultar(), "idSetor", "nome", produto.idSetor);
            return View(produto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("idProd,nome,desc,preco,idSetor")] Produto produto)
        {    
            if (id != produto.idProd)
                 return RedirectToAction("Erro", "Error", new {erro = 3});

            if (ModelState.IsValid && Formulario.ValidaPreco(produto))
            {
                try
                {
                    await prodRepository.Atualizar(produto);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!prodRepository.Verificar(produto.idProd))
                         return RedirectToAction("Erro", "Error", new {erro = 3});
                    else
                        throw;
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["idSetor"] = new SelectList(setorRepository.Consultar(), "idSetor", "nome", produto.idSetor);
            return View(produto);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return RedirectToAction("Erro", "Error", new {erro = 1});

            
            var produto = await prodRepository.Detalhes(id);

            if (produto == null)
                return RedirectToAction("Erro", "Error", new {erro = 2});

            return View(produto);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try{
                
                await produtoRepository.Deletar(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return RedirectToAction("Erro", "Error", new {erro = ""});
            }
        }
    }
}
