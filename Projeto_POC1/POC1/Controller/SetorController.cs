using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using POC1.Data;
using POC1.Models;
using POC1.Servicos;

namespace POC1.Controllers
{
    public class SetorController : BaseController
    {
        public SetorController(LojaContext context) :base(context)
        {
        }
        public IActionResult Index(int pag = 1, string setor="Setor")
        {
            ViewData["Setor"] = setor;

            var query = filtro.Filtrar(setor);

            ViewBag.TotalPaginas = Paginacao.ContaLista(query);
            return View(Paginacao.NumeroItems(query,pag));
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("idSetor,nome")] Setor setor)
        {
            try{
                if (ModelState.IsValid)
                {

                    await setorRepository.Criar(setor);
                    return RedirectToAction(nameof(Index));
                }
                return View(setor);
            }
            catch
            {
                return RedirectToAction("Erro", "Error", new {erro = ""});
            }
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
                return RedirectToAction("Erro", "Error", new {erro = 1});

        
            var setor = await setorRepository.Detalhes(id);

            if (setor == null)
                return RedirectToAction("Erro", "Error", new {erro = 4});

            return View(setor);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("idSetor,nome")] Setor setor)
        {
            if (id != setor.SetorId)
                return RedirectToAction("Erro", "Error", new {erro = 1});

            if (ModelState.IsValid)
            {
                try
                {
                    await setorRepository.Atualizar(setor);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!setorRepository.Verificar(setor.SetorId))
                        return RedirectToAction("Erro", "Error", new {erro = 1});
                    else
                        throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(setor);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return RedirectToAction("Erro", "Error", new {erro = 1});

            var setor = await setorRepository.Detalhes(id);
            
            if (setor == null)
                return RedirectToAction("Erro", "Error", new {erro = 4});

            return View(setor);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try{
                await setorRepository.Deletar(id);
                return RedirectToAction(nameof(Index));
            }
            catch{
                return RedirectToAction("Erro", "Error");
            }
        }
    }
}
