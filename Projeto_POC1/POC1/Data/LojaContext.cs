using Microsoft.EntityFrameworkCore;
using POC1.Models;

namespace POC1.Data
{
    public class LojaContext :DbContext
    {
        public LojaContext(DbContextOptions<LojaContext> options) : base(options)
        {   
            
        } 
        public DbSet<Produto> Produto{get;set;}
        public DbSet<Setor> Setor{get;set;}
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Loja.db");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           modelBuilder.ApplyConfiguration(new ProdutoConfiguration());
           modelBuilder.ApplyConfiguration(new SetorConfiguration());
        }
    }
}