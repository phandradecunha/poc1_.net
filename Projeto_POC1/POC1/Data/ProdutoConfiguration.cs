using Microsoft.EntityFrameworkCore;
using POC1.Models;

namespace POC1.Data
{
    public class ProdutoConfiguration : IEntityTypeConfiguration<Produto>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Produto> builder)
        {
            builder.Property(p=>p.Nome).IsRequired(true);
            builder.Property(p=>p.Preco).IsRequired(true).HasColumnType("decimal(5,2)");
            builder.Property(p=>p.Descricao).IsRequired(false);
            builder.HasOne(p=>p.Setor).WithMany(s=>s.Produto).HasForeignKey("SetorId");
            
        }
    }
}