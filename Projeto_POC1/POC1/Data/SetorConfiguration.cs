using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using POC1.Models;

namespace POC1.Data
{
    public class SetorConfiguration : IEntityTypeConfiguration<Setor>
    {
        public void Configure(EntityTypeBuilder<Setor> builder)
        {
            builder.Property(s=>s.Nome).IsRequired(true);
            builder.HasMany(g => g.Produto).WithOne(s=>s.Setor).HasForeignKey("idSetor").OnDelete(DeleteBehavior.Cascade);
        }
    }
}