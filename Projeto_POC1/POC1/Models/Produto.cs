using System;
using System.ComponentModel.DataAnnotations;

namespace POC1.Models
{
    public class Produto
    {
        public int idProd{get;set;}
        [Display(Name="Produto")]
        public string nome{get;set;}
        [Display(Name="Descrição")]
        public string Descricao {get;set;}

        [DataType(DataType.Currency), 
        DisplayFormat(DataFormatString = "{0:C2}"),
        Display(Name="Preço")]
        public decimal Preco{get;set;}
        public Nullable<int> idSetor { get; set; }
        public virtual Setor Setor{get;set;}
    }
}