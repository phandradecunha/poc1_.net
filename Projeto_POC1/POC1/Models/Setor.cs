using System.Collections.Generic;

namespace POC1.Models
{
    public class Setor
    {
        public int idSetor{get;set;}
        public string nome{get;set;}

        public virtual ICollection<Produto> Produto{get;set;}
    }
}