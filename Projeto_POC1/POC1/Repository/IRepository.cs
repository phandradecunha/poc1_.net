using System.Collections.Generic;
using System.Threading.Tasks;

namespace POC1
{
    public interface IRepository<T>
    {
         IEnumerable<T> Consultar();
         Task<T> Detalhes(int? id);

         bool Verificar(int? id);

         Task Criar(object valor);

         Task Atualizar(object valor);

         Task Deletar(int id);
    }
}