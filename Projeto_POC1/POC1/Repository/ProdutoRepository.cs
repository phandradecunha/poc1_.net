using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using POC1.Data;
using POC1.Models;
using System.Linq;


namespace POC1.DAO
{
    public class ProdutoRepository : IRepository<Produto> 
    { 
        private readonly LojaContext _context;
        public ProdutoRepository(LojaContext context)
        {
            _context = context;
        }

        public IEnumerable<Produto> Consultar(){
            return  _context.Produto.Include(p => p.Setor);
        }

        
        public async Task<Produto> Detalhes(int? id){
            var produto = _context.Produto.Include(p => p.Setor).FirstOrDefaultAsync(m => m.ProdutoId == id);
            return await produto;
        } 

        
        public bool Verificar(int? id){
           return _context.Produto.Any(e => e.ProdutoId == id);
        }
        
        
        public async Task Criar(object produto){
            _context.Add(produto);
            await _context.SaveChangesAsync();

        }
        
        
        public async Task Atualizar(object produto){
            _context.Update(produto);
            await _context.SaveChangesAsync();
        }

        
        public async Task Deletar(int id){
            var produto = _context.Produto.Find(id);
            _context.Remove(produto);
            await _context.SaveChangesAsync();
        }
        
    }
}
