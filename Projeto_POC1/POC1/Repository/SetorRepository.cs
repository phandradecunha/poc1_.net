using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using POC1.Data;
using POC1.Models;

namespace POC1.DAO
{
    public class SetorRepository : IRepository<Setor>
    {
        private readonly LojaContext _context;
        public SetorRepository(LojaContext context)
        {
            _context = context;
        }
        public IEnumerable<Setor> Consultar(){
            return _context.Setor;
        }

        public async Task<Setor> Detalhes(int? id){
            var setor = _context.Setor.FirstOrDefaultAsync(m => m.SetorId == id);
            return await setor;
        }
        
        public async Task Criar(object setor){
            _context.Add(setor);
            await _context.SaveChangesAsync();
            
        }

        public async Task Atualizar(object setor){
            _context.Update(setor);
            await _context.SaveChangesAsync();
            
        }

        public async Task Deletar(int id){
            var setor = _context.Setor.Find(id);
            _context.Remove(setor);
            await _context.SaveChangesAsync();
            
        }

        public bool Verificar(int? id){
            return _context.Setor.Any(e => e.SetorId == id);
        }
    }
}