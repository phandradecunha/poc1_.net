using System.Collections.Generic;
using System.Linq;
using POC1.Controllers;
using POC1.DAO;
using POC1.Data;
using POC1.Models;

namespace POC1.Servicos
{
    public class Filtro 
    {
        private ProdutoRepository produtoRepository;
        private SetorRepository setorRepository;

        public Filtro(LojaContext context)
        {
            produtoRepository = new ProdutoRepository(context);
            setorRepository = new SetorRepository(context);
        }

        //Filtro de Produtos
        public List<Produto> Filtrar(string nome, string setor){
            var query = produtoRepository.Consultar();

            //Define um padrao de consulta
            string nomeUpper = "";
            if (nome!=null){
                nomeUpper = nome.ToUpper();
            }

            //Define o filtro de produtos com base nos valores passados
            if (nome != null && setor != "Setor")
            {
                query = query.Where(q=> q.Nome.ToUpper().Contains(nomeUpper) && q.Setor.Nome == setor);
            }
            else if(nome != null)
            {
                query = query.Where(q=> q.Nome.ToUpper().Contains(nomeUpper));
            }
            else if(setor!="Setor")
            {
                query = query.Where(q=> q.Setor.Nome == setor);  
            }

            return query.ToList();
        }

        //Filtro de Setor
        public List<Setor> Filtrar(string setor){

            //faz uma consulta geral
            var query = setorRepository.Consultar();

            string setorUpper = "";
            if (setor != "Setor"){
                //Define o padrao de consulta
                setorUpper = setor.ToUpper();
                //Consulta
                query = query.Where(q=> q.Nome.ToUpper().Contains(setorUpper));
            }
            
            return query.ToList();
        }
    }
}