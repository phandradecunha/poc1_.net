using System.Collections.Generic;
using System.Linq;

namespace POC1.Servicos
{
    public static class Paginacao
    {

        //Conta o numero de items da lista
        public static double ContaLista<T>(List<T> lista){
            return lista.Count();
        }
        
        //Faz o calculo para alterar os produtos mostrados conforme a página que voce se encontra
        public static IEnumerable<T> NumeroItems<T>(List<T> lista, int pag){
            var skip = (pag*10) - 10;
            return lista.Skip(skip).Reverse().Take(10);
        }

    }
}