#pragma checksum "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\Erro.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "74bd62afb1be366ef17b94522bc0c4fc6f3154b2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Erro), @"mvc.1.0.view", @"/Views/Shared/Erro.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\macru\Desktop\GSW\POC1\Views\_ViewImports.cshtml"
using POC1;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\macru\Desktop\GSW\POC1\Views\_ViewImports.cshtml"
using POC1.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"74bd62afb1be366ef17b94522bc0c4fc6f3154b2", @"/Views/Shared/Erro.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3b0f6637fd7c22997075c862e12c0ab96d4ffd23", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Erro : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ErrorViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\Erro.cshtml"
  
    ViewData["Title"] = "Error";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1 class=\"text-danger\">Erro.</h1>\r\n<h2 class=\"text-danger\">Um erro occoreu enquanto processavamos seu pedido.</h2>\r\n\r\n");
#nullable restore
#line 9 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\Erro.cshtml"
 if (Model.ShowRequestId)
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <p>\r\n        <strong>Request ID:</strong> <code>");
#nullable restore
#line 12 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\Erro.cshtml"
                                      Write(Model.RequestId);

#line default
#line hidden
#nullable disable
            WriteLiteral("</code>\r\n    </p>\r\n    <p>\r\n        <h5>Detalhes:");
#nullable restore
#line 15 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\Erro.cshtml"
                Write(Model.Message);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n    </p>\r\n");
#nullable restore
#line 17 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\Erro.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("}\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ErrorViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
