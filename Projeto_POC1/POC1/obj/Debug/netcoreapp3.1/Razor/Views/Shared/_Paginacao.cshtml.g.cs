#pragma checksum "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\_Paginacao.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "dae49e1b2f9c375ddba9429bf4ecf1e09a2e970c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__Paginacao), @"mvc.1.0.view", @"/Views/Shared/_Paginacao.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\macru\Desktop\GSW\POC1\Views\_ViewImports.cshtml"
using POC1;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\macru\Desktop\GSW\POC1\Views\_ViewImports.cshtml"
using POC1.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"dae49e1b2f9c375ddba9429bf4ecf1e09a2e970c", @"/Views/Shared/_Paginacao.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3b0f6637fd7c22997075c862e12c0ab96d4ffd23", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__Paginacao : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\_Paginacao.cshtml"
   
    var controllerName = this.ViewContext.RouteData.Values["controller"].ToString();
    var actionName = this.ViewContext.RouteData.Values["action"].ToString();
 

#line default
#line hidden
#nullable disable
            WriteLiteral(" <div class=\"pagination\">\r\n");
#nullable restore
#line 6 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\_Paginacao.cshtml"
      
      var nPag = Math.Ceiling((double)ViewBag.TotalPaginas/10);
      for (int i=1;i<=nPag;i++){

#line default
#line hidden
#nullable disable
            WriteLiteral("        <a");
            BeginWriteAttribute("href", " href=", 317, "", 431, 1);
#nullable restore
#line 9 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\_Paginacao.cshtml"
WriteAttributeValue("", 323, Url.Action( actionName, controllerName, new { pag = i , setor=ViewData["Setor"], nome = ViewData["Nome"] }), 323, 108, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 9 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\_Paginacao.cshtml"
                                                                                                                        Write(i);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n");
#nullable restore
#line 10 "C:\Users\macru\Desktop\GSW\POC1\Views\Shared\_Paginacao.cshtml"
      }
      
    

#line default
#line hidden
#nullable disable
            WriteLiteral("  </div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
