**Projeto POC 1**
Projeto realizado na GSW para colocar os assuntos estudados pela equipe .NET em prática.

**Sobre o projeto**
O projeto consiste em simular uma carrinho de compras de uma loja, onde existe o CRUD para os produtos e setores.

**Ferramentas utilizadas**
*  ASP .NET CORE
*  HTML
*  Orientação a objetos
*  Framework Entity